#include <Wire.h>
#include "PS2X_lib.h"  //for v1.6
//#include <Servo.h>   //include servo lib
#include "Adafruit_MotorShield.h"
#include "Adafruit_MS_PWMServoDriver.h"

/******************************************************************
   set pins connected to PS2 controller:
     - 1e column: original`
     - 2e colmun: Stef?
   replace pin numbers by the ones you use
 ******************************************************************/
/******************************************************************
   select modes of PS2 controller:
     - pressures = analog reading of push-butttons
     - rumble    = motor rumbling
   uncomment 1 of the lines for each mode selection
 ******************************************************************/
#define pressures   true
#define rumble      true

#define PS2_DAT        12
#define PS2_CMD        11
#define PS2_SEL        10
#define PS2_CLK        13

Adafruit_MotorShield AFMS = Adafruit_MotorShield();


PS2X ps2x; // create PS2 Controller Class

Adafruit_Servo *ServoG = AFMS.getServo(1);
Adafruit_Servo *ServoFS = AFMS.getServo(2);
Adafruit_Servo *ServoRS = AFMS.getServo(3);
Adafruit_Servo *ServoR = AFMS.getServo(4);

Adafruit_DCMotor *L_M1 = AFMS.getMotor(1);
Adafruit_DCMotor *R_M2 = AFMS.getMotor(2);

int FBUDone = 90;
int rot = 90;
int grab = 90;
int FBUDtwo = 90;

//right now, the library does NOT support hot pluggable controllers, meaning
//you must always either restart your Arduino after you connect the controller,
//or call config_gamepad(pins) again after connecting the controller.

int error = 0;
byte type = 0;
byte vibrate = 0;

void setup() {
  Serial.begin(57600);

  AFMS.begin();

  L_M1->run(RELEASE);
  R_M2->run(RELEASE);

  Serial.println("All four servo motors have been configured.");

  delay(300);  //added delay to give wireless ps2 module some time to startup, before configuring it

  //  //CHANGES for v1.6 HERE!!! **************PAY ATTENTION*************
  //
  //  //setup pins and settings: GamePad(clock, command, attention, data, Pressures?, Rumble?) check for error
  //  error = ps2x.config_gamepad(PS2_CLK, PS2_CMD, PS2_SEL, PS2_DAT, pressures, rumble);

  do {
    //setup pins and settings: GamePad(clock, command, attention, data, Pressures?, Rumble?) check for error
    error = ps2x.config_gamepad(PS2_CLK, PS2_CMD, PS2_SEL, PS2_DAT, pressures, rumble);
    if (error == 0) {
      Serial.println("Configured successful");
      break;
    } else {
      Serial.print(".");
      delay(100);
    }
  } while (1);

  type = ps2x.readType();
  switch (type) {
    case 0:
      Serial.println("Unknown Controller type found ");
      break;
    case 1:
      Serial.println("DualShock Controller found ");
      break;
    case 2:
      Serial.println("GuitarHero Controller found ");
      break;
    case 3:
      Serial.println("Wireless Sony DualShock Controller found ");
      break;
  }
}

void loop() {
  /* You must Read Gamepad to get new values and set vibration values. ps2x.read_gamepad(small motor on/off, larger motor strenght from 0-255).
     If you don't enable the rumble, use ps2x.read_gamepad(); with no values.
     You should call this at least once a second
  */

  if (error == 1) //skip loop if no controller found
    return;

  //DualShock Controller
  ps2x.read_gamepad(false, vibrate); //read controller and set large motor to spin at 'vibrate' speed

  ServoG->writeServo(grab);
  ServoFS->writeServo(FBUDone);
  ServoRS->writeServo(FBUDtwo);
  ServoR->writeServo(rot);

  if (ps2x.Button(PSB_START))       //will be TRUE as long as button is pressed
    Serial.println("Start is being held");

  if (ps2x.Analog(PSS_LY) == 127 || ps2x.Analog(PSS_LY) == 128) {
    L_M1->run(RELEASE);
  }
  else if (ps2x.Analog(PSS_LY) > 128) {
    L_M1->setSpeed(255);
    L_M1->run(BACKWARD);
    Serial.print("Left Y stick: ");
    Serial.println(ps2x.Analog(PSS_LY));
    Serial.println("Left: B");
  }
  else if (ps2x.Analog(PSS_LY) < 127) {
    L_M1->setSpeed(255);
    L_M1->run(FORWARD);
    Serial.print("Left Y stick: ");
    Serial.println(ps2x.Analog(PSS_LY));
    Serial.println("Left: F");
  }

  if (ps2x.Analog(PSS_RY) == 127 || ps2x.Analog(PSS_RY) == 128) {
    R_M2->run(RELEASE);
  }
  else if (ps2x.Analog(PSS_RY) > 128) {
    R_M2->setSpeed(255);
    R_M2->run(BACKWARD);
    Serial.print("Right Y stick: ");
    Serial.println(ps2x.Analog(PSS_RY));
    Serial.println("Right: B");
  }
  else if (ps2x.Analog(PSS_RY) < 127) {
    R_M2->setSpeed(255);
    R_M2->run(FORWARD);
    Serial.print("Right Y stick: ");
    Serial.println(ps2x.Analog(PSS_RY));
    Serial.println("Right: F");
  }

  if (ps2x.Button(PSB_PAD_UP)) {
    Serial.println("Down");
  }
  if (ps2x.Button(PSB_PAD_RIGHT)) {
    rot = rot + 1;
    Serial.println("Right");
  }
  if (ps2x.Button(PSB_PAD_LEFT)) {
    rot = rot - 1;
    Serial.println("Left");
  }
  if (ps2x.Button(PSB_PAD_DOWN)) {
    Serial.println("Up");
  }

  if (ps2x.Button(PSB_L2)) {
    grab = grab + 1;
           Serial.println("Close");
  }
  if (ps2x.Button(PSB_R2)) {
    grab = grab - 1;
           Serial.println("Open");
  }

  if (ps2x.Button(PSB_L1)) {
  }
  if (ps2x.Button(PSB_R1)) {
  }
  delay(50);
}
